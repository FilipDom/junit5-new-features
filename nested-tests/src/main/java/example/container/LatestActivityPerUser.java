package example.container;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.function.BinaryOperator;

public class LatestActivityPerUser implements Iterable<ActivityEvent> {
	
	private static Comparator<ActivityEvent> latestActivityComparator = Comparator.comparing(ActivityEvent::getDate, Comparator.reverseOrder());
	
	private Map<String, ActivityEvent> activityPerUser = new HashMap<>();

	public Optional<ActivityEvent> add(ActivityEvent activity) {
		String user = activity.getUser();
		LatestActivityMerger latestActivityMerger = new LatestActivityMerger();
		activityPerUser.merge(user, activity, latestActivityMerger);
		return latestActivityMerger.getOlderActivity();
	}

	private static class LatestActivityMerger implements BinaryOperator<ActivityEvent> {

		private ActivityEvent olderActivity;
		
		@Override
		public ActivityEvent apply(ActivityEvent presentActivity, ActivityEvent activity) {
			ActivityEvent latestActivity;
			if (latestActivityComparator.compare(activity, presentActivity) < 0) {
				latestActivity = activity;
				olderActivity = presentActivity;
			} else {
				latestActivity = presentActivity;
				olderActivity = activity;
			}
			return latestActivity;
		}
		
		public Optional<ActivityEvent> getOlderActivity() {
			return Optional.ofNullable(olderActivity);
		}
	}
	
	@Override
	public Iterator<ActivityEvent> iterator() {
		return activityPerUser.values().iterator();
	}
	
}
