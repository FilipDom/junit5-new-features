package example.container;

import java.time.LocalDateTime;
import java.util.Objects;

public class ActivityEvent {

	private final String user;
	private final LocalDateTime date;
	
	public ActivityEvent(String user, LocalDateTime date) {
		this.user = user;
		this.date = date;
	}
	
	public String getUser() {
		return user;
	}
	
	public LocalDateTime getDate() {
		return date;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(date, user);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof ActivityEvent))
			return false;
		ActivityEvent other = (ActivityEvent) obj;
		return Objects.equals(date, other.date) && Objects.equals(user, other.user);
	}

	@Override
	public String toString() {
		return "ActivityEvent [user=" + user + ", date=" + date + "]";
	}
	
}
