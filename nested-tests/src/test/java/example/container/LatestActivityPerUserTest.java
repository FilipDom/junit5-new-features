package example.container;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class LatestActivityPerUserTest {

	private static final LocalDateTime ACTIVITY_TIME = LocalDateTime.of(2020, 1, 6, 21, 32);
	private static final LocalDateTime OLDER_ACTIVITY_TIME = ACTIVITY_TIME.minusDays(1);
	private static final LocalDateTime NEWER_ACTIVITY_TIME = ACTIVITY_TIME.plusDays(1);
	
	private LatestActivityPerUser latestActivities;

	@BeforeEach
	public void setUp() {
		latestActivities = new LatestActivityPerUser();
	}

	@Test
	public void add_whenEmpty_thenEmptyOptional() {
		// given
		ActivityEvent activity = createEventForUser1(ACTIVITY_TIME);
		// when
		Optional<ActivityEvent> oldActivity = latestActivities.add(activity);
		// then
		assertThat(oldActivity).isEmpty();
	}

	@Test
	public void iterator_whenEmpty_thenEmpty() {
		// then
		assertThat(latestActivities).isEmpty();
	}

	@Nested
	class WhenOneActivityPresent {
		@BeforeEach
		public void setUp() {
			// given
			ActivityEvent activity = createEventForUser1(ACTIVITY_TIME);
			latestActivities.add(activity);
		}

		@Nested
		@DisplayName("add")
		class Add {

			@Test
			public void whenOlderActivity_thenOlderActivity() {
				// given
				ActivityEvent olderActivity = createEventForUser1(OLDER_ACTIVITY_TIME);
				// when
				Optional<ActivityEvent> oldActivityActual = latestActivities.add(olderActivity);
				// then
				assertThat(oldActivityActual).contains(olderActivity);
			}

			@Test
			public void whenNewerActivity_thenPresentActivity() {
				// given
				ActivityEvent newerActivity = createEventForUser1(NEWER_ACTIVITY_TIME);
				// when
				Optional<ActivityEvent> oldActivityActual = latestActivities.add(newerActivity);
				// then
				assertThat(oldActivityActual).map(ActivityEvent::getDate).contains(ACTIVITY_TIME);
			}
			
			@Test
			public void whenOtherUserActivity_thenEmptyOptional() {
				// given
				ActivityEvent otherUserActivity = new ActivityEvent("user2", NEWER_ACTIVITY_TIME);
				// when
				Optional<ActivityEvent> oldActivityActual = latestActivities.add(otherUserActivity);
				// then
				assertThat(oldActivityActual).isEmpty();
			}
		}
		
		@Nested
		@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
		class Iterator {
			@Test
			public void thenOneActivity() {
				// then
				assertIterableEquals(Arrays.asList(createEventForUser1(ACTIVITY_TIME)), latestActivities);
			}
			
			@Test
			public void when_older_activity_added_then_present_activity() {
				// given
				ActivityEvent olderActivity = createEventForUser1(OLDER_ACTIVITY_TIME);
				latestActivities.add(olderActivity);
				// then
				assertIterableEquals(Arrays.asList(createEventForUser1(ACTIVITY_TIME)), latestActivities);
			}
			
			@Test
			public void when_newer_activity_added_then_newer_activity() {
				// given
				ActivityEvent newerActivity = createEventForUser1(NEWER_ACTIVITY_TIME);
				latestActivities.add(newerActivity);
				// then
				assertIterableEquals(Arrays.asList(createEventForUser1(NEWER_ACTIVITY_TIME)), latestActivities);
			}
			
			@Test
			public void when_other_user_activity_added_then_both_activities() {
				// given
				ActivityEvent otherUserActivity = new ActivityEvent("user2", NEWER_ACTIVITY_TIME);
				latestActivities.add(otherUserActivity);
				// then
				assertThat(latestActivities).containsExactlyInAnyOrder(createEventForUser1(ACTIVITY_TIME), otherUserActivity);
			}
		}
	}

	private ActivityEvent createEventForUser1(LocalDateTime activityTime) {
		return new ActivityEvent("user1", activityTime);
	}

}
