package example.joint;

import static org.junit.Assert.*;

import org.junit.Test;

public class IdentityFunctionJunit4Test {

	@Test
	public void resultShouldBeTheSameAsArgument() {
		//given
		IdentityFunction idFunc = new IdentityFunction();
		String orig = "abc";
		//when
		String result = idFunc.apply(orig);
		//then
		assertEquals(orig, result);
	}

}
