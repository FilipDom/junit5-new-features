package example.joint;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class IdentityFunctionJupiterTest {

	@Test
	public void resultShouldBeTheSameAsArgument() {
		//given
		IdentityFunction idFunc = new IdentityFunction();
		String orig = "abc";
		//when
		String result = idFunc.apply(orig);
		//then
		assertEquals(orig, result);
	}

}
