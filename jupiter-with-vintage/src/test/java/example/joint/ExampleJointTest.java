package example.joint;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.function.IntBinaryOperator;

import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ExampleJointTest {
	
	private IntBinaryOperator func;
	
	@Before
	public void setUpJunit4() {
		func = Integer::sum;
	}
	
	@BeforeEach
	public void setUpJupiter() {
		func = Integer::max;
	}

	@Test
	public void maxOf5And2ShouldBe5() {
		//given
		int operand1 = 5;
		int operand2 = 2;
		//when
		int max = func.applyAsInt(operand1, operand2);
		//then
		assertEquals(5, max);
	}
	
	@org.junit.Test
	public void sumOf5And2ShouldBe7() {
		//given
		int operand1 = 5;
		int operand2 = 2;
		//when
		int sum = func.applyAsInt(operand1, operand2);
		//then
		assertEquals(7, sum);
	}

}
