# Using Jupiter Together with JUnit 4

[User Guide - Maven Settings](https://junit.org/junit5/docs/current/user-guide/#running-tests-build-maven)

To run older JUnit 4 Tests along the new Jupiter tests, JUnit 4 and the vintage-engine must be listed as dependencies:

```
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.13</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>org.junit.vintage</groupId>
        <artifactId>junit-vintage-engine</artifactId>
        <scope>test</scope>
    </dependency>
```

When executing the tests with maven, they JUnit 4 tests will now be executed as well. 

## Adding JUnit 5 Support to Existing Projects Using JUnit 4

In an existing project, already using JUnit 4 it is impractical to suddenly migrate all tests to JUnit 5. Fortunately, that should not be necessary. Old and new tests can coexist in the same project. The old tests are run using the junit-vintage-engine. 

To add support for JUnit 5 the following dependencies need to be added:
* **org.junit.jupiter:junit-jupiter-api**
* **org.junit.jupiter:junit-jupiter-engine**
* **org.junit.vintage:junit-vintage-engine**

Additionally, **maven-surefire-plugin** and **maven-failsafe-plugin** need to have at least version **2.22.2**. 
After the adjustment, the pom should look somewhat like this:

```
<project>
  ...
  <dependencyManagement>
    <dependencies>
      ...
      <dependency>
        <groupId>org.junit</groupId>
        <artifactId>junit-bom</artifactId>
        <version>5.6.2</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
      ...
    </dependencies>
  </dependencyManagement>
  ...
  <dependencies>
    ...
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-api</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-engine</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>org.junit.vintage</groupId>
        <artifactId>junit-vintage-engine</artifactId>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.13</version>
        <scope>test</scope>
    </dependency>
    ...
  </dependencies>
  ...
  <build>
    ...
    <pluginManagement>
      <plugins>
        ...
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-surefire-plugin</artifactId>
          <version>2.22.2</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-failsafe-plugin</artifactId>
          <version>2.22.2</version>
        </plugin>
        ...
      </plugins>
    </pluginManagement>
    ...
  </build>
  ...
</project>

```

### Recommended Steps When Adding JUnit 5 Support

In order to make sure everything is configured properly, the following steps, at minimum, can be used:
* Before making any changes to the configuration or adding new tests, run all of the tests in your project, for example with `mvn clean test`
* Make note of the number of run tests. Depending on your configuration, you can take a look at the created reports or search for the following text in the console output. If your project consists of multiple modules, there will be several such sections:

    [INFO] -------------------------------------------------------
    [INFO]  T E S T S
    [INFO] -------------------------------------------------------
    [INFO] Running example.basic.IdentityFunctionTest
    [INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.051 s - in example.basic.IdentityFunctionTest
    ...
    [INFO]
    [INFO] Results:
    [INFO]
    [INFO] Tests run: 15, Failures: 0, Errors: 0, Skipped: 0

* Make the pom adjustments as described above
* Run the tests again and compare the results. If the number of run tests is the same, proceed ahead. 
* Add a new JUnit 5 test
* Run the tests again and make sure the new test was run as well.  

## Common Gotchas when Using JUnit 4 and 5 in the Same Project

1. Mixing classes when Using JUnit 4 and 5 in the Same Project. 

   Most of the new classes reside in the `org.junit.jupiter.api` package, while most of the old classes can be found under `org.junit`. 
   
   When both types of annotations are found, the respective tests are run separately by the vintage-engine and the jupiter-engine. Since most of the annotations do not have a meaning in the other version, they are ignored. This can lead to confusing errors, such as null pointer exceptions, although it looks like the field should be initialised in the set-up method. 
   
   **Common mixups:**
   
   Most other annotations have a different name now, but the **@Test** annotation exists in both packages and it is easy to import the wrong one. 
   
   Another common one is `org.junit.Assert` and `org.junit.jupiter.api.Assertions`. This one is not as problematic. The first inconvenience is that one might be searching for some of the new assertion methods, such as assertThrows, in the wrong class. 
   
   The second problem is that relying on Assert carries a potential future migration burden. If at a later point it is desired to completely remove JUnit 4 from the dependencies, this causes additional migration work. On the other hand, there should be no downsides to using Assertions instead. 

   The main exception is that Assertions lacks the `assertThat` method, which works with **Hamcrest** matchers. However, `org.hamcrest.MatcherAssert.assertThat` is available instead. 
   
   **Symptoms:** tests not run; null pointer exception due to uninitialised fields; seemingly missing assertion methods

2. Forgetting to update maven-failsafe-plugin and maven-surefire-plugin.

   It is common to add the JUnit 5 dependencies are added, but forget to update the plugins. 
   
   **Symptoms:** everything seems to work, but upon closer observation, the new JUnit 5 tests are not run

