# JUnit 5 Configuration

[User Guide - Maven Settings](https://junit.org/junit5/docs/current/user-guide/#running-tests-build-maven)

To add support for JUnit 5 the following dependencies need to be added:
* **org.junit.jupiter:junit-jupiter-api**
* **org.junit.jupiter:junit-jupiter-engine**

Additionally, **maven-surefire-plugin** and **maven-failsafe-plugin** need to have at least version **2.22.2**. 
After the adjustment, the pom should look somewhat like this:

```
<project>
  ...
  <dependencyManagement>
    <dependencies>
      ...
      <dependency>
        <groupId>org.junit</groupId>
        <artifactId>junit-bom</artifactId>
        <version>5.6.2</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
      ...
    </dependencies>
  </dependencyManagement>
  ...
  <dependencies>
    ...
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-api</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.junit.jupiter</groupId>
      <artifactId>junit-jupiter-engine</artifactId>
      <scope>test</scope>
    </dependency>
    ...
  </dependencies>
  ...
  <build>
    ...
    <pluginManagement>
      <plugins>
        ...
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-surefire-plugin</artifactId>
          <version>2.22.2</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-failsafe-plugin</artifactId>
          <version>2.22.2</version>
        </plugin>
        ...
      </plugins>
    </pluginManagement>
    ...
  </build>
  ...
</project>

```

To check that everything work properly, run the tests via the command line, for example `mvn clean test`. Do not rely on IDEs for this, the tests will most likely run in them, even if everything is not fully configured. 