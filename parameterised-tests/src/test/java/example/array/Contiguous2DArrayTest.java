package example.array;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.stream.Stream;

import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

public class Contiguous2DArrayTest {
	
	public static Stream<Arguments> givenDimensions() {
		return Stream.of(
				arguments(1, 2, 2),
				arguments(2, 1, 2),
				arguments(5, 3, 15),
				arguments(4, 8, 32)
				);
	}
	
	@ParameterizedTest
	@MethodSource("givenDimensions")
	public void whenCreatedWithDimensions_thenNumberOfElementsEqualToProductOfDimensions(int rowCount, int colCount, int expectedElementCount) {
		//given
		Contiguous2DArray array2d = new Contiguous2DArray(rowCount, colCount);
		//when
		int elementCount = array2d.getNumberOfElements();
		//then
		assertEquals(expectedElementCount, elementCount);
	}

	@ParameterizedTest
	@MethodSource("givenDimensions")
	public void whenCreatedWithDimensions_thenRowCountEqualToRowCount(int rowCount, int colCount) {
	//given
			Contiguous2DArray array2d = new Contiguous2DArray(rowCount, colCount);
			//when
			int actualRowCount = array2d.getRowCount();
			//then
			assertEquals(rowCount, actualRowCount);
	}
	
	@ParameterizedTest
	@MethodSource("givenDimensions")
	public void whenCreatedWithDimensions_thenColCountEqualToColCount(int rowCount, int colCount) {
			//given
			Contiguous2DArray array2d = new Contiguous2DArray(rowCount, colCount);
			//when
			int actualColCount = array2d.getColCount();
			//then
			assertEquals(colCount, actualColCount);
	}
	
	public static Stream<Arguments> givenIllegalDimensions() {
		return Stream.of(
				arguments(-1, 2),
				arguments(2, -1),
				arguments(-3, -2)
				);
	}
	
	@ParameterizedTest
	@CsvSource({
			"-1,2",
			"2,-1",
			"-3,-2"
			})
	public void whenCreatedWithIllegalDimensions_thenThrowException(int rowCount, int colCount) {
		//when		
		Executable createNewArray = () -> new Contiguous2DArray(rowCount, colCount);
		//then
		assertThrows(IllegalArgumentException.class, createNewArray);
	}
	
	public static Stream<Arguments> givenCoordsAndIncrement() {
		return Stream.of(
				arguments(2, 3, 0, 0, 1),
				arguments(3, 2, 2, 1, 4),
				arguments(2, 3, 1, 2, -5)
				);
	}
	
	@ParameterizedTest
	@MethodSource("givenCoordsAndIncrement")
	public void add_whenEmpty_thenIncrement(int rowCount, int colCount, int rowIdx, int colIdx, int increment) {
		//given
		Contiguous2DArray array2d = new Contiguous2DArray(rowCount, colCount);
		//when
		long valAfterAdd = array2d.add(increment, rowIdx, colIdx);
		//then
		assertEquals(increment, valAfterAdd);
	}
	
	public static Stream<Arguments> givenCoordsValAndIncrement() {
		return Stream.of(
				arguments(2, 3, 0, 0, 1, 1, 2),
				arguments(3, 5, 2, 4, 3, -2, 1),
				arguments(5, 3, 4, 2, 2, 3, 5),
				arguments(1, 1, 0, 0, 4, 1, 5)
				);
	}
	
	@ParameterizedTest
	@MethodSource("givenCoordsValAndIncrement")
	public void add_whenVal_thenPresentValPlusIncrement(int rowCount, int colCount, int rowIdx, int colIdx, int val, int increment, int expectedValAfterAdd) {
		//given
		Contiguous2DArray array2d = new Contiguous2DArray(rowCount, colCount);
		array2d.add(val, rowIdx, colIdx);
		//when
		long valAfterAdd = array2d.add(increment, rowIdx, colIdx);
		//then
		assertEquals(expectedValAfterAdd, valAfterAdd);
	}
}
