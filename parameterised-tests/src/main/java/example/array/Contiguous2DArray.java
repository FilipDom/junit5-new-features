package example.array;

public class Contiguous2DArray {

	private int rowCount;
	private int colCount;
	
	private long[] array;
	
	public Contiguous2DArray(int rowCount, int colCount) {
		if (rowCount < 0 || colCount < 0) {
			String errorMessage = String.format("rowCount and colCount must be non-negative. Received (%d, %d)", rowCount, colCount);
			throw new IllegalArgumentException(errorMessage);
		}
		this.rowCount = rowCount;
		this.colCount = colCount;
		this.array = new long[getNumberOfElements()];
	}

	public int getNumberOfElements() {
		return rowCount * colCount;
	}

	public int getRowCount() {
		return rowCount;
	}

	public int getColCount() {
		return colCount;
	}

	public long add(int val, int rowIdx, int colIdx) {
		int idx = calcIdx(rowIdx, colIdx);
		array[idx] += val;
		return array[idx];
	}

	private int calcIdx(int rowIdx, int colIdx) {
		return rowIdx*colCount + colIdx;
	}
}
